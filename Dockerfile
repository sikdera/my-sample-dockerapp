FROM docker.io/centos
MAINTAINER “animesh.sikder@gmail.com"
RUN yum install -y httpd git 
RUN git clone https://gitlab.com/sikdera/my-sample-dockerapp.git /var/www/html
EXPOSE 80
CMD ["/usr/sbin/httpd","-D","FOREGROUND"]